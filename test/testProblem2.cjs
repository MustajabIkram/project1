const problem2 = require("../problem2.cjs")

let cars = require("../cars.js");
let inventory = cars.inventory;

const result = problem2(inventory)

if(result.length !== 0){
    console.log(`Last car is a ${result.car_make} ${result.car_model}`)
} else{
    console.log(result)
}
