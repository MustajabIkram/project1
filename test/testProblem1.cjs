const problem1 = require("../problem1.cjs");

let cars = require("../data/cars.cjs");
let lessCars = require("../data/lessCars.cjs")
let inventory = cars.inventory;
let lessInventory = lessCars.lessInventory

let searchId = 33;
let nothing = []

const result = problem1()
if(((typeof result) === "object" || (typeof result) === Array) && result.length !== 0) {
    console.log([`Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`])
} else{
    console.log(nothing)
}

const result1 = problem1(new String("hello"), 33)
if(((typeof result1) === "object" || (typeof result1) === Array) && result1.length !== 0) {
    console.log([`Car ${result1.id} is a ${result1.car_year} ${result1.car_make} ${result1.car_model}`])
} else{
    console.log(nothing)
}

const result2 = problem1(inventory, 33)
if(((typeof result2) === "object" || (typeof result2) === Array) && result2.length !== 0) {
    console.log([`Car ${result2.id} is a ${result2.car_year} ${result2.car_make} ${result2.car_model}`])
} else{
    console.log(nothing)
}

const result3 = problem1(inventory, [])
if(((typeof result3) === "object" || (typeof result3) === Array) && result3.length !== 0) {
    console.log([`Car ${result3.id} is a ${result3.car_year} ${result3.car_make} ${result3.car_model}`])
} else{
    console.log(nothing)
}

const result4 = problem1(lessInventory, 20)
if(((typeof result4) === "object" || (typeof result4) === Array) && result4.length !== 0) {
    console.log([`Car ${result4.id} is a ${result4.car_year} ${result4.car_make} ${result4.car_model}`])
} else{
    console.log(nothing)
}