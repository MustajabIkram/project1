const problem3 = require("../problem3.cjs")

let cars = require("../cars.js");
let inventory = cars.inventory;

const result = problem3(inventory)

if(result.length !== 0){
    console.log(result)
} else{
    console.log("Error Accessing Inventory")
}
