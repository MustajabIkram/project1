const problem4 = require("../problem4.cjs")

let cars = require("../cars.js");
let inventory = cars.inventory;

const result = problem4(inventory);
if(result.length !== 0) {
    console.log(result)
} else {
    console.log("Inventory not accessible");
}