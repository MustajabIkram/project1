const problem6 = require("../problem6.cjs") 

let cars = require("../cars.js");
let inventory = cars.inventory;

const result =  problem6(inventory);
if(result.length !== 0) {
    for(let i = 0; i < result.length; i++) {
        console.log(JSON.stringify(result[i]))
    }
} else {
    console.log("Error while accessing Inventory")
}