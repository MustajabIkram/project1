const problem4 = require("./problem4.cjs")
const problem5 = (inventory) => {
    const result = problem4(inventory);
    if((result !== undefined) && (result.length!==0)) {
        let yearlessthen2000 = []
        for(let i = 0; i < result.length; i++){
            if(result[i] < 2000){
                yearlessthen2000.push(result[i])
            }
        }
        return yearlessthen2000
    } else{
        return []
    }
}

module.exports = problem5;
