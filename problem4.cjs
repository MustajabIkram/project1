const problem4 = (inventory) => {
    if((inventory !== undefined) && (inventory.length !== 0)){
        let carsYearArray = []
        for (let i = 0; i < inventory.length; i++){
            carsYearArray.push(inventory[i].car_year)
        }
        return carsYearArray
    } else {
        return []
    }
}

module.exports = problem4;