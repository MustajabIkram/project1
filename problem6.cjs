const problem6 = (inventory) => {
    if((inventory !== undefined) && (inventory.length !== 0)) {
        let modelsBMWandAudi = []
        for(let i = 0; i < inventory.length; i++){
            if((inventory[i].car_make === 'BMW') || (inventory[i].car_make === 'Audi'))
            modelsBMWandAudi.push(inventory[i])
        }
        return modelsBMWandAudi
    } else {
        return []
    }
}

module.exports = problem6;
