const problem1 = (inventory, searchID) => {
    if (!(inventory instanceof Array)) {
        return []
    }
    if ((typeof searchID) !== (typeof 1)) {
        return []
    }
    if((typeof inventory) === "object" || (typeof inventory) === Array) {
        try {
            if((searchID !== undefined) && (inventory !== undefined)) {
                for (let i = 0; i < inventory.length; i++) {
                    if (inventory[i].id === searchID) {
                        return inventory[i];
                    }
                }
            } else {
                return []
            }
        } catch (e) {
            return []
        }
    } else {
        return []
    }
}
module.exports = problem1;