const problem3 = (inventory) => {
    if((inventory !== undefined) && (inventory.length !== 0)){
        let models = []
        for(let i = 0; i < inventory.length; i++){
            models.push(inventory[i].car_model)
        }
        let sortedOrder = models.sort()
        return sortedOrder
    } else {
        return []
    }
}

module.exports = problem3;
