exports.lessInventory = [{"id":1,"car_make":"Lincoln","car_model":"Navigator","car_year":2009},
{"id":2,"car_make":"Mazda","car_model":"Miata MX-5","car_year":2001},
{"id":3,"car_make":"Land Rover","car_model":"Defender Ice Edition","car_year":2010},
{"id":4,"car_make":"Honda","car_model":"Accord","car_year":1983},
{"id":5,"car_make":"Mitsubishi","car_model":"Galant","car_year":1990},
{"id":6,"car_make":"Audi","car_model":"riolet","car_year":1995},
{"id":7,"car_make":"Smart","car_model":"Fortwo","car_year":2009},
{"id":8,"car_make":"Audi","car_model":"4000CS Quattro","car_year":1987},
{"id":9,"car_make":"Ford","car_model":"Windstar","car_year":1996},
{"id":10,"car_make":"Mercedes-Benz","car_model":"E-Class","car_year":2000},
{"id":11,"car_make":"Infiniti","car_model":"G35","car_year":2004},
{"id":12,"car_make":"Lotus","car_model":"Esprit","car_year":2004},
{"id":13,"car_make":"Chevrolet","car_model":"Cavalier","car_year":1997},
{"id":14,"car_make":"Dodge","car_model":"Ram Van 1500","car_year":1999},
{"id":15,"car_make":"Dodge","car_model":"Intrepid","car_year":2000},
{"id":16,"car_make":"Mitsubishi","car_model":"Montero Sport","car_year":2001},
{"id":17,"car_make":"Buick","car_model":"Skylark","car_year":1987},
{"id":18,"car_make":"Geo","car_model":"Prizm","car_year":1995},
{"id":19,"car_make":"Oldsmobile","car_model":"Bravada","car_year":1994},
{"id":20,"car_make":"Mazda","car_model":"Familia","car_year":1985},
{"id":21,"car_make":"Chevrolet","car_model":"Express 1500","car_year":2003},
{"id":22,"car_make":"Jeep","car_model":"Wrangler","car_year":1997},
{"id":23,"car_make":"Eagle","car_model":"Talon","car_year":1992},
{"id":24,"car_make":"Toyota","car_model":"MR2","car_year":2003}];